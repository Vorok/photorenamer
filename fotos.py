#!/usr/bin/env python3

# El programara extraerá los metadatos de una fotografía y la renombrará en función de los datos de la fecha cuando fue realizada.

import os
import exifread
import datetime
import re

def list_photos():
    filtered=[]
    try:
        images=os.listdir()
        for image in images:
            if all(x in image for x in [pattern, imageformat]):
                filtered.append(image)
    except:
        raise("Error genérico.")
    return filtered

def clean_datetime(dirty_dt):
    purged_dt = []
    for number in dirty_dt:
        if number == '00':
            purged_dt.append('0')
        else:
            purged_dt.append(number.lstrip("0"))
    return purged_dt

def extract_metadata(images_list):
    dicc_images_datetimes = {}
    try:
        for image in images_list:
            with open(image,'rb') as current:
                format1 = re.sub('[:-]',',',str(exifread.process_file(current)['EXIF DateTimeOriginal']))
                format2 = list(format1.replace(' ',',').split(','))
                formatted = clean_datetime(format2)
                original_datetime = datetime.datetime(int(formatted[0]), int(formatted[1]), int(formatted[2]), int(formatted[3]), int(formatted[4]), int(formatted[5]))
                dicc_images_datetimes[image] = original_datetime
        return dicc_images_datetimes
    except:
        raise("Error genérico.")
    
def process_offset(offset, dicc_images_datetimes):
    dicc_im_dt_offs = {}
    hh = int(offset.split(':')[0])
    mm = int(offset.split(':')[1])
    ss = int(offset.split(':')[2])
    formatted_offset = datetime.timedelta(seconds = ss, minutes = mm, hours = hh)
    for image,original_datetime in dicc_images_datetimes.items():
        final_datetime = (original_datetime + formatted_offset)
        fdate = str(final_datetime).split(' ')[0].replace('-','')
        ftime = str(final_datetime).split(' ')[1].replace(':','')
        dicc_im_dt_offs[image] = fdate+'-'+ftime
    return dicc_im_dt_offs

def rename_files(offset_dict):
    try:
        for image,time in offset_dict.items():
            newname = newnamepattern+time.split('-')[0]+"_"+time.split('-')[1]+"."+imageformat
            print("El nombre de la imagen "+image+" será: "+ newname)
            os.rename(image, newname)
    except:
        raise("Error genérico.")

def __main__():
    try:
        os.chdir(folder)
        print(os.getcwd())
    except OSError:
        raise OSError()
    images_list = list_photos()
    metadata_dict = extract_metadata(images_list)
    print(metadata_dict)
    if (offset != ""):
        formatted_offset = process_offset(offset, metadata_dict)
        rename_files(formatted_offset)
    else:
        rename_files(images_list)

folder = input("Indica el directorio donde se encuentran la/s fotografía/s -> ")
pattern = input("Indica el patrón del nombre de las fotografías a editar -> ")
imageformat = input("Indica el formato que tienen las imágenes -> ")
offset = input("Indica el descuadre en las horas de las fotos en formato hh:mm:ss -> ")
newnamepattern = input("Indica el patrón inicial del nuevo nombre de las fotografías -> ")
# folder = "/home/anto/Imágenes/test_fotospy/"
# pattern = "DSC"
# imageformat = "JPG"
# offset = "16:50:00"
# newnamepattern = ""

if __name__ == "__main__":
    __main__()
